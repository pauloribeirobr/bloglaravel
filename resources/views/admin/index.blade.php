@extends('admin._template')

@section('titulo', 'Página Inicial')

@section('body')
        <div class="container-fluid">
            <div class="col-md-8 col-md-offset-2">

                @if(session('alertaOK'))
                    @include('_alertaOK')
                @endif

                @if(session('alertaERRO'))
                    @include('_alertaERRO')
                @endif

                <ol class="breadcrumb">
                    <li class="active"><i class="fa fa-home" aria-hidden="true"></i> Início</li>
                </ol>
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-link-azul">
                            <a href="{{ url('/admin/cadastros') }}">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-folder fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>Cadastros</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-link-azul">
                            <a href="{{ url('/admin/estoques') }}">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-cubes fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>Estoques</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-link-azul">
                            <a href="{{ url('/admin/config') }}">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-cogs fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>Configurações</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-link-azul">
                            <a href="{{ url('/admin/config') }}">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-cloud fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div>Diversos</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endsection
