@extends('admin._template')

@section('titulo', 'Dados da Linguagem')

@section('body')
    <div class="container-fluid">
            <div class="col-md-4 col-md-offset-4">
                @if(session('alertaOK'))
                    @include('_alertaOK')
                @endif
                @if(session('alertaERRO'))
                    @include('_alertaERRO')
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <i class="fa fa-language" aria-hidden="true"></i> Dados da Linguagem
                        </h5>
                    </div>
                    <div class="panel-body">
                        <form method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if( isset($linguagem) )
                                <input type="hidden" id="id" name="id" value="{{ $linguagem->id }}">
                            @endif
                            <div class="form-group {{ $errors->first('nome') ? 'has-error' : '' }}">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="{{ $linguagem->nome or old('nome') }}" placeholder="Nome">
                            </div>
                            <div class="form-group {{ $errors->first('url') ? 'has-error' : '' }}">
                                <label for="nome">Url</label>
                                <input type="text" name="url" id="url" class="form-control" value="{{ $linguagem->url or old('url') }}" placeholder="Url">
                            </div>
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default" href="{{ route('_linguagens') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                                </div>
                                @if(isset($linguagem))
                                    <div class="btn-group">
                                        <a class="btn btn-danger" href="{{ route('_linguagens_apagar', ['id' => $linguagem->id]) }}"><span class="glyphicon glyphicon-trash"></span> Apagar</a>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
