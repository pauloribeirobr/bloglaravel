@extends('admin._template')

@section('titulo', 'Página Inicial')

@section('body')
    <div class="container-fluid">
        <div class="col-md-4 col-md-offset-4">
            @if(session('alertaOK'))
                @include('_alertaOK')
            @endif
            @if(session('alertaERRO'))
                @include('_alertaERRO')
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <i class="fa fa-language" aria-hidden="true"></i> Linguagens <a class="panel-title-button btn btn-danger btn-xs" href="{{ route('_linguagens_incluir') }}"> <span class="glyphicon glyphicon-plus-sign"></span> incluir</a>
                    </h5>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    {{--<th>Código</th>--}}
                                    <th>Linguagem</th>
                                    <th>Url</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($linguagens) > 0)
                                    @foreach($linguagens as $linguagem)
                                        <tr>
                                            {{--<td>{{ $linguagem->id }}</td>--}}
                                            <td>
                                                <a href="{{ route('_linguagens_dados', ['id' => $linguagem->id]) }}">{{ $linguagem->nome }}</a>
                                            </td>
                                            <td>{{ $linguagem->url }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3" class="text-center">Nenhuma Linguagem Cadastrada</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
