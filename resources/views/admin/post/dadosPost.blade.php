@extends('admin._template')

@section('titulo', 'Dados do Post')

@section('body')
    <div class="container-fluid">
            <div class="col-md-10 col-md-offset-1">
                @if(session('alertaOK'))
                    @include('_alertaOK')
                @endif
                @if(session('alertaERRO'))
                    @include('_alertaERRO')
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <i class="fa fa-file-text" aria-hidden="true"></i> Dados do Post
                        </h5>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
                            <form method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if( isset($post) )
                                    <input type="hidden" id="id" name="id" value="{{ $post->id }}">
                                @endif
                                <div class="form-group {{ $errors->first('nome') ? 'has-error' : '' }}">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="nome" id="nome" class="form-control" value="{{ $post->nome or old('nome') }}" placeholder="Título">
                                </div>
                                <div class="form-group {{ $errors->first('url') ? 'has-error' : '' }}">
                                    <label for="nome">Url</label>
                                    <input type="text" name="url" id="url" class="form-control" value="{{ $post->url or old('url') }}" placeholder="Url">
                                </div>
                                <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                                    <label for="linguagem">Linguagem</label>
                                    <select class="form-control" id="linguagem" name="linguagem">
                                        @foreach ($linguagens as $linguagem)
                                            <option value="{{ $linguagem->id }}"
                                                @if( isset($post) )
                                                    {{ $post->linguagem_id == $linguagem->id ? ' selected' : '' }}
                                                @else
                                                    {{ old('linguagem') == $linguagem->id ? ' selected' : '' }}
                                                @endif
                                                >{{ $linguagem->nome }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div cass="form-group">
                                    <p><b>Categorias</b></p>
                                    <div id="htmlCategorias">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="resumo">Frase de Chamada</label>
                                    <textarea name="resumo" id="resumo" class="form-control" rows="5">{{ $post->resumo or old('resumo') }}</textarea>
                                </div>
                                <div class="form-group {{ $errors->first('conteudo') ? 'has-error' : '' }}">
                                    <label for="texto">Texto</label>
                                    <textarea name="texto" id="texto" class="form-control" rows="25">{{ $post->texto or old('conteudo') }}</textarea>
                                </div>
                                <div class="form-group {{ $errors->first('usuario') ? 'has-error' : '' }}">
                                    <label for="usuario">Autor</label>
                                    <select class="form-control" id="usuario" name="usuario">
                                        @foreach ($usuarios as $usuario)
                                            <option value="{{ $usuario->id }}"
                                                @if( isset($post) )
                                                    {{ $post->usuario_id == $usuario->id ? ' selected' : '' }}
                                                @else
                                                    {{ old('usuario') == $usuario->id ? ' selected' : '' }}
                                                @endif
                                                >{{ $usuario->nome }}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="publicado">
                                            <input type="checkbox" name="publicado" id="publicado"
                                            @if(isset($post))
                                                {{ $post->publicado ? "checked" : ""}}
                                            @else
                                                {{ old('publicado') ? "checked" : ""}}
                                            @endif
                                            value="publicado"> Publicado
                                        </label>
                                    </div>
                                </div>

                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-default" href="{{ route('_posts') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                                    </div>
                                    @if(isset($categoria))
                                        <div class="btn-group">
                                            <a class="btn btn-danger" href="{{ route('_posts_apagar', ['id' => $categoria->id]) }}"><span class="glyphicon glyphicon-trash"></span> Apagar</a>
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">

                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

<script type="text/javascript">
    $(document).ready(function(){
        getCategorias($('#linguagem').val(), $('#id').val());
        $('#linguagem').change(function() {
            getCategorias($('#linguagem').val(), $('#id').val());
        });
    });

    tinymce.init({
        selector: '#texto',
        menubar: false,
        toolbar: ' bold italic | link image | alignleft aligncenter alignright | bullist numlist outdent indent | style-p style-h2 style-h3 style-h4 style-h5 style-h6 fullscreen code',
        statusbar: false,
        plugins: "stylebuttons, fullscreen, code, paste, link, image",
        content_css: [
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
        ],
        apply_source_formatting : true,
        paste_auto_cleanup_on_paste : true,
        paste_remove_styles: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: "all",
        entity_encoding : "raw",
        file_browser_callback: function(field_name, url, type, win) {
            //tinymce.activeEditor.windowManager.close();
            //console.log(field_name);
            tinymce.activeEditor.windowManager.open({
                title: 'Inserir Imagem',
                file: "/admin/imagens",
                width: 600,
                height: 400,
                resizable : "yes",
                inline : "yes",
                close_previous : "no",
                buttons: [{
                    text: 'Insert',
                    classes: 'widget btn primary first abs-layout-item',
                    disabled: false,
                    onclick: 'close'
                }, {
                    text: 'Close',
                    onclick: 'close',
                    window : win,
                    input : field_name
                }]
            });
            return false;
        },

    });
    tinyMCE.PluginManager.add('stylebuttons', function(editor, url) {
        ['pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(name){
            editor.addButton("style-" + name, {
                tooltip: "Toggle " + name,
                text: name.toUpperCase(),
                onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                onPostRender: function() {
                    var self = this, setup = function() {
                        editor.formatter.formatChanged(name, function(state) {
                            self.active(state);
                        });
                    };
                    editor.formatter ? setup() : editor.on('init', setup);
                }
            })
        });
    });

    function myBlogBrowser() {
        alert("Oi");
    }

    function getCategorias(linguagem_id, post_id) {
        htmlCategorias = '';
        $('#htmlCategorias').html('');
        $.getJSON('/admin/categorias/api/'+ linguagem_id +'/' + post_id, function(dados) {
            $.each(dados,function(indice, valor){
                htmlCategorias += '<div class="checkbox">';
                htmlCategorias += '<label><input type="checkbox" value="'+ valor.id +'" '+ (valor.is_post ? 'checked="checked"' : '') +' onchange="checkCategoria({{ $post->id or -1 }}, '+ valor.id +')">'+ valor.nome +'</label>';
                htmlCategorias += '</div>';
            });
            $('#htmlCategorias').html(htmlCategorias);
            console.log(dados);
            console.log(htmlCategorias);

        });
    }

    function checkCategoria(post, categoria) {
        if(post == -1) {
            alert("Salve o Post antes de marcar as Categorias!");
            return false;
        }
       $.post("/admin/categorias/api/check",
       {
           "_token": "{{ csrf_token() }}",
           post_id: post,
           categoria_id: categoria
       },
       function(data, status){
           getCategorias($('#linguagem').val(), $('#id').val());
       });
    }

</script>

@endsection
