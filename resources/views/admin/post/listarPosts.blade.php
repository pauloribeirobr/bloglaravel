@extends('admin._template')

@section('titulo', 'Posts')

@section('body')
    <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
            @if(session('alertaOK'))
                @include('_alertaOK')
            @endif
            @if(session('alertaERRO'))
                @include('_alertaERRO')
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <i class="fa fa-file-text" aria-hidden="true"></i> Posts <a class="panel-title-button btn btn-danger btn-xs" href="{{ route('_posts_incluir') }}"> <span class="glyphicon glyphicon-plus-sign"></span> incluir</a>
                    </h5>
                </div>
                <div class="panel-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                            <label for="linguagem">Selecione a Linguagem</label>
                            <select class="form-control" id="linguagem" name="linguagem" onchange="this.form.submit()">
                                <option value="0" {{ session()->get('filtroLinguagem') == 0 ? ' selected' : '' }}>Todas</option>
                                @foreach ($linguagens as $linguagem)
                                    <option value="{{ $linguagem->id }}"
                                        {{ session()->get('filtroLinguagem') == $linguagem->id ? ' selected' : '' }}>{{ $linguagem->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    {{--<th>id</th>
                                    <th>Ordem</th>--}}
                                    <th>Post</th>
                                    <th>Url</th>
                                    <th>Linguagem</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($posts) > 0)
                                    @foreach($posts as $post)
                                        <tr>
                                            {{--<td>{{ $post->id }}</td>
                                            <td>{{ $post->ordem }}</td>--}}
                                            <td>
                                                <a href="{{ route('_posts_dados', ['id' => $post->id]) }}">{{ $post->nome }}</a>
                                            </td>
                                            <td>{{ $post->url }}</td>
                                            <td>{{ $post->linguagem->nome }}</td>
                                            <td class="text-center">
                                                {!! $post->publicado ? '<span class="label label-success">publicado</span>' : '<span class="label label-danger">rascunho</span>' !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Nenhuma Post Cadastrado</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
