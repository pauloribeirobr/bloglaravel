@extends('admin._template')

@section('titulo', 'Imagens')

@section('body')
    <div class="container-fluid">
        <div class="col-md-8 col-md-offset-2">
            @if(session('alertaOK'))
                @include('_alertaOK')
            @endif
            @if(session('alertaERRO'))
                @include('_alertaERRO')
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <i class="fa fa-picture-o" aria-hidden="true"></i> Imagens
                    </h5>
                </div>
                <div class="panel-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                            <label for="linguagem">Selecione a Linguagem</label>
                            <select class="form-control" id="linguagem" name="linguagem" onchange="this.form.submit()">
                                <option value="0" {{ session()->get('filtroLinguagem') == 0 ? ' selected' : '' }}>Todas</option>
                                @foreach ($linguagens as $linguagem)
                                    <option value="{{ $linguagem->id }}"
                                        {{ session()->get('filtroLinguagem') == $linguagem->id ? ' selected' : '' }}>{{ $linguagem->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group {{ $errors->first('post') ? 'has-error' : '' }}">
                            <label for="post">Selecione um Post</label>
                            <select class="form-control" id="post" name="post" onchange="this.form.submit()">
                                <option value="0" {{ session()->get('filtroPost') == 0 ? ' selected' : '' }}>Todos</option>
                                @foreach ($posts as $post)
                                    <option value="{{ $post->id }}"
                                        {{ session()->get('filtroPost') == $post->id ? ' selected' : '' }}>{{ $post->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <span class="btn btn-success fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Selecionar imagem</span>
                                        <input id="imagem" type="file" accept=".jpg, .jpeg, .png" name="imagem" data-token="{{ csrf_token() }}">
                                    </span>
                                    <br>
                                    <br>
                                    <div id="progressImagem" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <div class="alert alert-warning" role="alert" id="statusUpload"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @foreach ($imagens as $imagem)
                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
                            {{--<a href="javascript:void(0)" onclick="editaImagem({{$imagem->id}});">--}}
                            <a href="{{ route('_imagens_dados', ['id' => $imagem->id])}}">
                                <img src="{{url("/img/thumb/" . $imagem->id . '.jpg')}}" class="img-responsive thumbnail" alt="{{ $imagem->texto }}" title="{{ $imagem->texto }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {{-- Modal de Edicao da Imagem --}}
    <div class="modal fade" id="modalImagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                   <p id="modalImagemTitulo"></p>
                </h4>
             </div>
             <div class="modal-body">
                 <div class="panel panel-primary">
                     <div class="panel-body">
                         <div class="col-md-8 col-lg-8 col-sm-6 col-xs-12">
                             <img id="modalImagemSrc" class="img-responsive thumbnail">
                         </div>
                         <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                             <div class="panel panel-default">
                                 <div class="panel-body">
                                 {{--<p><b>{{ $imagem->nome }}</b></p>--}}
                                 <p>altura:  <b id="modalImagemAltura"></b></p>
                                 <p>largura: <b id="modalImagemLargura"></b></p>
                                 <p>tamanho: </p>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="modalBotaoPreco">Atualizar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
             </div>
          </div>
       </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('adm/js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('adm/js/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('adm/js/jquery.fileupload.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#statusUpload').hide();
            if($('#linguagem').val() == 0) {
                $("#imagem").attr("disabled", "disabled");
            }else{
                $("#imagem").removeAttr("disabled");
            }
        });

        $(function () {
            'use strict';
            $('#imagem').fileupload({
                url: '{{ route("_imagens_upload") }}',
                formData: {_token: $('#imagem').data('token')},
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progressImagem .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                    $('#statusUpload').html('Por favor aguarde o processamento do arquivo...');
                    $('#statusUpload').show(500);

                },
                done: function (e, data) {
                    $('#progressImagem .progress-bar').css('width', '0%').attr('aria-valuenow', 0);
                    console.log(data.result.mensagem);
                    if(data.result.status == 'TRUE') {
                        document.location='{{route('_imagens')}}';
                        //$('#statusUpload').html(data.result.mensagem);
                    }else{
                        $('#statusUpload').html(data.result.mensagem);
                    }
                }
            });
        });

        function editaImagem(id) {
            $.getJSON('/admin/imagens/api/dados/' + id, function (dados) {
                $("#modalImagemTitulo").html(dados.nome);
                $("#modalImagemSrc").attr('src', dados.caminho_completo);
                $("#modalImagemAltura").html(dados.altura + " px");
                $("#modalImagemLargura").html(dados.largura + " px");
                //$("#modalAlteraPrecoTitulo").html(data[0].produto_nome);
                //$("#modalAlteraPrecoTituloTabela").html('Tabela ' + data[0].tabela_nome);
                //var venda = parseFloat(data[0].venda,10).toFixed(2);
                //$("#modalValor").val(venda);
            });
            $('#modalImagem').modal('show');
        }
    </script>

@endsection
