@extends('admin._template')

@section('titulo', 'Dados da Imagem')

@section('body')
    <div class="container-fluid">
            <div class="col-md-10 col-md-offset-1">
                @if(session('alertaOK'))
                    @include('_alertaOK')
                @endif
                @if(session('alertaERRO'))
                    @include('_alertaERRO')
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <i class="fa fa-picture-o" aria-hidden="true"></i> Dados da Imagem
                        </h5>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6 col-lg-8 col-sm-6 col-xs-12">
                            <img src="{{url($imagem->caminho_completo)}}" class="img-responsive thumbnail">
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p>imagem:  <b>{{ $imagem->nome }}</b></p>
                                    <p>caminho: <b>{{ $imagem->caminho_completo }}</b></p>
                                    <p>altura:  <b>{{ $imagem->altura }} px</b></p>
                                    <p>largura: <b>{{ $imagem->largura }} px</b></p>
                                    <p>tamanho: <b>{{ number_format($imagem->tamanho,0,",",".") }} bytes</b></p>
                                    <form method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" id="id" name="id" value="{{ $imagem->id }}">
                                        <div class="form-group {{ $errors->first('texto') ? 'has-error' : '' }}">
                                            <label for="texto">Descrição</label>
                                            <input type="text" name="texto" id="texto" class="form-control" value="{{ $imagem->texto or old('texto') }}" placeholder="Texto Alternativo">
                                        </div>
                                        <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                                            <label for="linguagem">Linguagem</label>
                                            <select class="form-control" id="linguagem" name="linguagem">
                                                @foreach ($linguagens as $linguagem)
                                                    <option value="{{ $linguagem->id }}"
                                                        @if( isset($imagem) )
                                                            {{ $imagem->linguagem_id == $linguagem->id ? ' selected' : '' }}
                                                        @else
                                                            {{ old('linguagem') == $linguagem->id ? ' selected' : '' }}
                                                        @endif
                                                        >{{ $linguagem->nome }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group {{ $errors->first('usuario') ? 'has-error' : '' }}">
                                            <label for="usuario">Autor</label>
                                            <select class="form-control" id="usuario" name="usuario">
                                                @foreach ($usuarios as $usuario)
                                                    <option value="{{ $usuario->id }}"
                                                        @if( isset($imagem) )
                                                            {{ $imagem->usuario_id == $usuario->id ? ' selected' : '' }}
                                                        @else
                                                            {{ old('usuario') == $usuario->id ? ' selected' : '' }}
                                                        @endif
                                                        >{{ $usuario->nome }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="btn-group btn-group-justified">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn btn-default" href="{{ route('_imagens') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                                            </div>
                                            @if(isset($imagem))

                                                <div class="btn-group">
                                                    <a class="btn btn-danger" onclick="modalConfirma('Apaga a imagem {{ $imagem->nome }} ?',
                                                    '{{ route("_imagens_apagar", ["id" => $imagem->id ])}}');"><span class="glyphicon glyphicon-trash"></span> Apagar</a>
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
@endsection
