<!DOCTYPE html>
<html lang="pt_br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>[pRibeiro.net] - Login</title>
        {{-- CSS Bootstrap --}}
        <link href="{{ asset('adm/css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="{{ asset('adm/css/admin.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="col-md-4 col-md-offset-4">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div id="panel-login" class="panel-heading"><span class="glyphicon glyphicon-log-in"></span> Login</div>
                    <div class="panel-body">

                        @if(session('alertaOK'))
                            @include('_alertaOK')
                        @endif

                        @if(session('alertaERRO'))
                            @include('_alertaERRO')
                        @endif

                        <form role="form" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                    <input type="text" name="email" id="email" class="form-control"
                                           value="{{ old('nome') }}" placeholder="Seu Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                                    <input type="password" name="senha" id="senha" class="form-control"
                                           value="{{ old('senha') }}" placeholder="Sua Senha">
                                </div>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <button class="btn btn-danger btn-block" type="submit"><span
                                            class="glyphicon glyphicon-log-in"></span> Entrar
                                    </button>
                                </div>
                            </div>
                        </form>
                        <a href="{{ route('_recuperaSenha') }}"> Esqueceu a Senha?</a>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('adm/js/jquery-2.2.4.min.js') }}"></script>
        <script src="{{ asset('adm/js/bootstrap.min.js') }}"></script>
    </body>
</html>
