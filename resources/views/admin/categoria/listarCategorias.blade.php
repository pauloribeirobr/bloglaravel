@extends('admin._template')

@section('titulo', 'Categorias')

@section('body')
    <div class="container-fluid">
        <div class="col-md-4 col-md-offset-4">
            @if(session('alertaOK'))
                @include('_alertaOK')
            @endif
            @if(session('alertaERRO'))
                @include('_alertaERRO')
            @endif
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> Categorias <a class="panel-title-button btn btn-danger btn-xs" href="{{ route('_categorias_incluir') }}"> <span class="glyphicon glyphicon-plus-sign"></span> incluir</a>
                    </h5>
                </div>
                <div class="panel-body">
                    <form method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                            <label for="linguagem">Selecione a Linguagem</label>
                            <select class="form-control" id="linguagem" name="linguagem" onchange="this.form.submit()">
                                <option value="0" {{ session()->get('filtroLinguagem') == 0 ? ' selected' : '' }}>Todas</option>
                                @foreach ($linguagens as $linguagem)
                                    <option value="{{ $linguagem->id }}"
                                        {{ session()->get('filtroLinguagem') == $linguagem->id ? ' selected' : '' }}>{{ $linguagem->nome }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    {{--<th>id</th>
                                    <th>Ordem</th>--}}
                                    <th>Categoria</th>
                                    <th>Url</th>
                                    <th>Linguagem</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($categorias) > 0)
                                    @foreach($categorias as $categoria)
                                        <tr>
                                            {{--<td>{{ $categoria->id }}</td>
                                            <td>{{ $categoria->ordem }}</td>--}}
                                            <td>
                                                <a href="{{ route('_categorias_dados', ['id' => $categoria->id]) }}">{{ $categoria->nome }}</a>
                                            </td>
                                            <td>{{ $categoria->url }}</td>
                                            <td>{{ $categoria->linguagem->nome }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    @if(!$loop->first && session()->get('filtroLinguagem') > 0)
                                                        <div class="btn-group">
                                                            <a class="btn btn-xs btn-primary" href="{{ route('_categorias_acima', ['id' => $categoria->id]) }}">
                                                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    @endif
                                                    @if(!$loop->last && session()->get('filtroLinguagem') > 0)
                                                        <div class="btn-group">
                                                            <a class="btn btn-xs btn-danger" href="{{ route('_categorias_abaixo', ['id' => $categoria->id]) }}">
                                                                <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Nenhuma Categoria Cadastrada</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
