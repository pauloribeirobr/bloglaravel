@extends('admin._template')

@section('titulo', 'Dados da Linguagem')

@section('body')
    <div class="container-fluid">
            <div class="col-md-8 col-md-offset-2">
                @if(session('alertaOK'))
                    @include('_alertaOK')
                @endif
                @if(session('alertaERRO'))
                    @include('_alertaERRO')
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> Dados da Categoria
                        </h5>
                    </div>
                    <div class="panel-body">
                        <form method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if( isset($categoria) )
                                <input type="hidden" id="id" name="id" value="{{ $categoria->id }}">
                            @endif
                            <div class="form-group {{ $errors->first('nome') ? 'has-error' : '' }}">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="{{ $categoria->nome or old('nome') }}" placeholder="Nome">
                            </div>
                            <div class="form-group {{ $errors->first('url') ? 'has-error' : '' }}">
                                <label for="nome">Url</label>
                                <input type="text" name="url" id="url" class="form-control" value="{{ $categoria->url or old('url') }}" placeholder="Url">
                            </div>
                            <div class="form-group {{ $errors->first('linguagem') ? 'has-error' : '' }}">
                                <label for="linguagem">Linguagem</label>
                                <select class="form-control" id="linguagem" name="linguagem">
                                    @foreach ($linguagens as $linguagem)
                                        <option value="{{ $linguagem->id }}"
                                            @if( isset($categoria) )
                                                {{ $categoria->linguagem_id == $linguagem->id ? ' selected' : '' }}
                                            @else
                                                {{ old('linguagem') == $linguagem->id ? ' selected' : '' }}
                                            @endif
                                            >{{ $linguagem->nome }}</option>
                                        @endforeach
                                </select>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            {{--<th>id</th>
                                            <th>Ordem</th>--}}
                                            <th>Post</th>
                                            <th>Url</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($posts) > 0)
                                            @foreach($posts as $post)
                                                <tr>
                                                    {{--<td>{{ $post->id }}</td>
                                                    <td>{{ $post->ordem }}</td>--}}
                                                    <td>
                                                        <a href="{{ route('_posts_dados', ['id' => $post->post->id]) }}">{{ $post->post->nome }}</a>
                                                    </td>
                                                    <td>{{ $post->post->url }}</td>
                                                    <td class="text-center">
                                                        {!! $post->post->publicado ? '<span class="label label-success">publicado</span>' : '<span class="label label-danger">rascunho</span>' !!}
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            @if(!$loop->first)
                                                                <div class="btn-group">
                                                                    <a class="btn btn-xs btn-primary" href="{{ route('_posts_categorias_acima', ['id' => $post->id]) }}">
                                                                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                            @if(!$loop->last)
                                                                <div class="btn-group">
                                                                    <a class="btn btn-xs btn-danger" href="{{ route('_posts_categorias_abaixo', ['id' => $post->id]) }}">
                                                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4" class="text-center">Nenhuma Post Cadastrado</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Salvar</button>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default" href="{{ route('_categorias') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                                </div>
                                @if(isset($categoria))
                                    <div class="btn-group">
                                        <a class="btn btn-danger" href="{{ route('_categorias_apagar', ['id' => $categoria->id]) }}"><span class="glyphicon glyphicon-trash"></span> Apagar</a>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
