@extends('blog._templateBlog')

@section('titulo', $post->nome)

@section('body')
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <h2>{{ $post->nome }}</h2>
            <div class="resumo">
                {!! $post->resumo !!}
            </div>
            <div class="texto">
                {!! $post->texto !!}
            </div>
        </div>
    </div>
@endsection
