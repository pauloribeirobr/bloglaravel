<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>@yield('titulo')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/estilo.css') }}" />
<style>
body {
    margin-top: 55px;
}
</style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"> Nome do Blog </a>

            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    {{--<li {{ request()->url() == route('_categorias') ? 'class=active' : '' }}><a href="{{ route('_categorias') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Categorias</a></li>
                    <li {{ request()->url() == route('_posts') ? 'class=active' : '' }}><a href="{{ route('_posts') }}"><i class="fa fa-file-text" aria-hidden="true"></i> Posts</a></li>--}}
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    {{--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-info-circle" aria-hidden="true"></i> Demais Dados <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li {{ request()->url() == route('_linguagens') ? 'class=active' : '' }}><a href="{{ route('_linguagens') }}"><i class="fa fa-language" aria-hidden="true"></i> Linguagens</a></li>
                            <li><a href="#"><i class="fa fa-users" aria-hidden="true"></i> Usuários</a></li>
                            <li><a href="{{ route('_logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Desconectar</a></li>
                        </ul></li>--}}
                </ul>
            </div>
        </div>
    </nav>
    @yield('body')
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@yield('scripts')
</html>
