<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('linguagem_id')->unsigned()->nullable();
            $table->foreign('linguagem_id')->references('id')->on('linguagens');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->integer('post_id')->unsigned()->nullable();
            $table->foreign('post_id')->references('id')->on('posts');
            $table->string('nome');
            $table->string('tipo');  //exemplo image/jpeg
            $table->string('caminho');
            $table->string('caminho_completo');
            $table->integer('altura');                       //altura (pixels)
            $table->integer('largura');                       //largura (pixels)
            $table->string('tamanho');                      //tamanho (Kb)
            $table->string('texto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagens');
    }
}
