<?php

use Illuminate\Database\Seeder;

class InicialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Tabela "usuarios"
        DB::table('usuarios')->insert([
            'id' => 1,
            'login' => 'paulo.ribeiro',
            'nome' => 'Paulo Ribeiro',
            'email' => 'pauloribeirobr@gmail.com',
            'senha' => bcrypt('123123'),
            'ativo' => TRUE
        ]);
    }
}
