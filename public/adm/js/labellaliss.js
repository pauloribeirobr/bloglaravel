/*
* Funcoes LaBella Liss
* Criado por Paulo Ribeiro em Agosto de 2016
*/

$(document).ready(function(){
    var offset              = 300; // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset_opacity      = 1200; //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    var scroll_top_duration = 700; //duration of the top scrolling animation (in ms)
    var $back_to_top        = $('.cd-top'); //grab the "back to top" link

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //seta para baixo e para cima em toogles
    $(".arrow-panel-toggle").addClass("glyphicon glyphicon-chevron-down");

    $(".arrow-panel-toggle").click(function () {
        $($(this).prev().attr("href")).collapse("toggle");
        toggleSetas($(this));
    });

    var list = $(".in");
    for (var i = 0; i < list.length; i++) {
        $($("a[href='#" + $(list[i]).attr("id") +
        "']")).next()
        .removeClass("glyphicon glyphicon-chevron-down")
        .addClass("glyphicon glyphicon-chevron-up");
    };

    $("a[data-toggle='collapse']").click(function () {
        // Change the glyphs
        toggleSetas($(this).next());
    });

    //go to top --
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
        }, scroll_top_duration
    );
});

    //navbar x social-links
    /*
    var menu = $('#navbar');
    var origOffsetY = menu.offset().top;

    function scroll() {
        if ($(window).scrollTop() >= origOffsetY) {
            //alert("Foi");
            $('#navbar').removeClass('navbar-static-top');
            $('#navbar').addClass('navbar-fixed-top');
            //$('.content').addClass('menu-padding');
        } else {
            $('#navbar').removeClass('navbar-fixed-top');
            $('#navbar').addClass('navbar-static-top');


            //$('.content').removeClass('menu-padding');
        }


    }

    document.onscroll = scroll;
    */
});
