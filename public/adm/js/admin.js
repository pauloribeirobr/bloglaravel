/*
 * Funcoes DucciGroup
 * Criado por Paulo Ribeiro em Junho de 2016
 */
//abre o Modal que existe no _template.html.twig para confirmar alguma operação
function modalConfirma(texto, url = '#', onclick = '') {
    $('#modalConfirmaTexto').text(texto);
    $('#modalConfirmaBotao').attr('href', url);
    if(onclick != '') {
        $('#modalConfirmaBotao').attr('onclick', onclick);
        $('#modalConfirmaBotao').attr('href', 'void()');
        $('#modalConfirmaBotao').attr('data-dismiss', 'modal');
    }
    $('#modalConfirma').modal('show');
}

//Abre o Modal Log
function modalLog(empresa_id, model, model_id) {
   //$('#modalLogTexto').text(
   consultaLog(empresa_id, model, model_id);
   //);
   $('#modalLog').modal('show');
}

//toogle Setas
function toggleSetas(toogle) {
  if ($(toogle).hasClass("glyphicon glyphicon-chevron-up")) {
    $(toogle).removeClass("glyphicon glyphicon-chevron-up");
    $(toogle).addClass("glyphicon glyphicon-chevron-down");
  }
  else {
    $(toogle).removeClass("glyphicon glyphicon-chevron-down");
    $(toogle).addClass("glyphicon glyphicon-chevron-up");
  }
}

//Mask Money - interessante
var mask = {
    money: function() {
        var el = this,exec = function(v) {
            v = v.replace(/\D/g,"");
            v = new String(Number(v));
            var len = v.length;
            if(len == 1) {
                v = v.replace(/(\d)/,"0,0$1");
            }
            if(len == 2) {
                v = v.replace(/(\d)/,"0,$1");
            }
            if (len > 2) {
                v = v.replace(/(\d{2})$/,',$1');
            }
            if (len > 5) {
                var x = len -5;
                var er = new RegExp('(\\d{'+ x +'})(\\d)');
                v = v.replace(er,'$1.$2');
            }
        return v;
        };

        setTimeout(function(){
            el.value = exec(el.value);
        },1);
    }
}

$(document).ready(function(){
    var offset              = 300; // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset_opacity      = 1200; //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    var scroll_top_duration = 700; //duration of the top scrolling animation (in ms)
    var $back_to_top        = $('.cd-top'); //grab the "back to top" link

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //seta para baixo e para cima em toogles
    $(".arrow-panel-toggle").addClass("glyphicon glyphicon-chevron-down");

    $(".arrow-panel-toggle").click(function () {
      $($(this).prev().attr("href")).collapse("toggle");
      toggleSetas($(this));
    });

    var list = $(".in");
    for (var i = 0; i < list.length; i++) {
        $($("a[href='#" + $(list[i]).attr("id") +
        "']")).next()
        .removeClass("glyphicon glyphicon-chevron-down")
        .addClass("glyphicon glyphicon-chevron-up");
    };

    $("a[data-toggle='collapse']").click(function () {
        // Change the glyphs
        toggleSetas($(this).next());
    });

    //go to top --
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
             }, scroll_top_duration
        );
    });
});
