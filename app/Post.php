<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['nome', 'url', 'linguagem_id', 'usuario_id', 'resumo', 'texto', 'publicado', 'ordem'];

    //Relaciona a Linguagem
    public function linguagem() {
        return $this->belongsTo(Linguagem::class);
    }

    //Relaciona o Usuário
    public function usuario() {
        return $this->belongsTo(Usuario::class);
    }

    public static function listar($linguagem_id = 0) {
        //if($linguagem_id == 0) {
        //    $posts = Post::orderBy('ordem','desc')->get();
        //}else{
            $posts = Post::where('linguagem_id','=',$linguagem_id)->orderBy('ordem', 'desc')->get();
        //}

        return $posts;
    }

    public static function dados($id) {
        $post = Post::where('id','=', $id)->first();
        return $post;
    }

    public static function gravar($dados) {

        $messages = [
            'required' => 'O campo :attribute precisa estar preenchido',
            'max' => 'O campo :attribute permite no máximo :max caracteres'
        ];

        $validator = Validator::make($dados, [
            'nome'      => 'required|max:200',
            'url'       => 'required|max:200',
            'linguagem' => 'required',
            'usuario'   => 'required',
            'texto'     => 'required'

        ], $messages);

        //Se alguma validação deu erro, retorna
        if ($validator->fails()) {
            return ['status' => FALSE, 'mensagem' => $validator];
        }

        //if(count($errors) > 0) {
        //    return ['status' => FALSE, 'mensagem' => $errors];
        //}

        if (isset($dados['id'])) {
            $post          = Post::where('id', '=', $dados['id'])->first();
        } else {
            $post          = new Post();
            $post->ordem   = (Post::where('linguagem_id','=', $dados['linguagem'])->count()) + 1;
        }

        $post->nome             = $dados['nome'];
        $post->url              = $dados['url'];
        $post->linguagem_id     = $dados['linguagem'];
        $post->usuario_id       = $dados['usuario'];
        $post->resumo           = $dados['resumo'];
        $post->texto            = $dados['texto'];
        $post->publicado            = $dados['publicado'];

        try {
            $post->save();
            return ['status' => TRUE, 'mensagem' => "Post " . $post->nome . " salvo!", 'id' => $post->id];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao salvar o Post. Avise a Área de TI. " .  $e->getMessage()];
        }
    }

    public static function apagar($id) {
        $post = Post::where('id','=', $id)->first();
        if (!$post) {
            return ['status' => FALSE, 'mensagem' => "Post não encontrado!"];
        }

        try {
            $post->delete();
            return ['status' => TRUE, 'mensagem' => "Post apagado!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Este Post está associado a um ou mais registros!"];
        }
    }

    public static function acima($post_id) {
        $post = Post::find($post_id);
        $ordem = $post->ordem;
        $postAnterior = Post::where('ordem','<', $ordem)
            ->where('linguagem_id','=', $post->linguagem_id)->orderBy('ordem','DESC')->first();
        if($postAnterior) {
            $post->ordem = $postAnterior->ordem;
            $postAnterior->ordem = $ordem;
            $post->save();
            $postAnterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }

    public static function abaixo($post_id) {
        $post = Post::find($post_id);
        $ordem = $post->ordem;
        $postPosterior = Post::where('ordem','>', $ordem)
            ->where('linguagem_id','=', $post->linguagem_id)->orderBy('ordem','ASC')->first();
        if($postPosterior) {
            $post->ordem = $postPosterior->ordem;
            $postPosterior->ordem = $ordem;
            $post->save();
            $postPosterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }
}
