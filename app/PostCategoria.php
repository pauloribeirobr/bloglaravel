<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
use App\Categoria;
use App\PostCategoria;
use DB;

class PostCategoria extends Model
{
    protected $table = 'posts_categorias';
    protected $fillable = ['post_id', 'categoria_id', 'ordem'];

    //Relaciona o Post
    public function post() {
        return $this->belongsTo(Post::class);
    }

    //Relaciona a Categoria
    public function categoria() {
        return $this->belongsTo(Categoria::class);
    }

    public static function dados($post_id, $categoria_id) {
        $postCategoria = PostCategoria::where('post_id','=', $post_id)
            ->where('categoria_id','=',$categoria_id)->first();
        if(!$postCategoria) {
            return FALSE;
        }
        return $postCategoria;
    }

    public static function totalPosts($categoria_id) {
        $totalPosts = PostCategoria::where('categoria_id','=', $categoria_id)->count();

        return $totalPosts;
    }

    public static function inserir($post_id, $categoria_id) {
        $categoria  = Categoria::dados($categoria_id);
        $post       = Post::dados($post_id);

        if(!$post || !$categoria) {
            return ['status' => FALSE];
        }

        $postCategoria = PostCategoria::dados($post->id, $categoria->id);

        if(!$postCategoria) {
            $postCategoria = new PostCategoria();
            $postCategoria->post_id = $post->id;
            $postCategoria->ordem = PostCategoria::totalPosts($categoria_id) + 1;
            $postCategoria->categoria_id = $categoria->id;

            try {
                $postCategoria->save();
                return ['status' => TRUE, 'mensagem' => "Post/Categoria associados!"];
            } catch (\Illuminate\Database\QueryException $e) {
                return ['status' => FALSE, 'mensagem' => "Problemas ao associar Post/Categoria. Avise a Área de TI. " .  $e->getMessage()];
            }
        }
        return ['status' => FALSE];
    }

    public static function remover($post_id, $categoria_id) {
        $postCategoria = PostCategoria::dados($post_id, $categoria_id);
        if(!$postCategoria) {
            return ['status' => FALSE];
        }

        try {
            $postCategoria->delete();
            return ['status' => TRUE, 'mensagem' => "Post/Categoria apagado!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao apagar Post/Categoria. Avise a Área de TI. " .  $e->getMessage()];
        }
    }

    public static function listarPorCategoria($categoria_id) {
        $postsCategorias = PostCategoria::where('categoria_id','=', $categoria_id)
            ->orderBy('ordem','asc')
            ->get();

        return $postsCategorias;
    }

    public static function listarCategorias($linguagem_id = -1, $post_id = -1) {
        $categorias = Categoria::where('linguagem_id','=', $linguagem_id)->orderBy('nome','asc')->get();

        foreach ($categorias as $categoria) {
            $postCategoria = PostCategoria::dados($post_id, $categoria->id);
            if($postCategoria) {
                $categoria->is_post = TRUE;
            }else{
                $categoria->is_post = FALSE;
            }

        }
        return $categorias->toJson();
    }

    public static function acima($postCategoria_id) {
        $postCategoria = PostCategoria::find($postCategoria_id);
        $ordem = $postCategoria->ordem;
        $postCategoriaAnterior = PostCategoria::where('ordem','<', $ordem)
            ->where('categoria_id','=', $postCategoria->categoria_id)->orderBy('ordem','DESC')->first();
        if($postCategoriaAnterior) {
            $postCategoria->ordem = $postCategoriaAnterior->ordem;
            $postCategoriaAnterior->ordem = $ordem;
            $postCategoria->save();
            $postCategoriaAnterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }

    public static function abaixo($postCategoria_id) {
        $postCategoria = PostCategoria::find($postCategoria_id);
        $ordem = $postCategoria->ordem;
        $postCategoriaPosterior = PostCategoria::where('ordem','>', $ordem)
            ->where('categoria_id','=', $postCategoria->categoria_id)->orderBy('ordem','ASC')->first();
        if($postCategoriaPosterior) {
            $postCategoria->ordem = $postCategoriaPosterior->ordem;
            $postCategoriaPosterior->ordem = $ordem;
            $postCategoria->save();
            $postCategoriaPosterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }

}
