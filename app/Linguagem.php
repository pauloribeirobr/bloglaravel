<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Linguagem extends Model
{
    protected $table = 'linguagens';
    protected $fillable = ['nome', 'url'];

    /**
    * Lista todas as Linguagens
    */
    public static function listar() {
        $linguagens = Linguagem::orderBy('nome', 'asc')->get();
        return $linguagens;
    }

    /**
    * Abre os dados da Linguagem
    */
    public static function dados($id) {
        $linguagem = Linguagem::where('id', '=', $id)->first();
        return $linguagem;
    }

    /**
    * Salva a Linguagem
    */
    public static function gravar($dados) {

        $messages = [
            'required' => 'O campo :attribute precisa estar preenchido',
            'max' => 'O campo :attribute permite no máximo :max caracteres'
        ];

        $validator = Validator::make($dados, [
            'nome'  => 'required|max:20',
            'url'   => 'required|max:5'
        ], $messages);

        //Se alguma validação deu erro, retorna
        if ($validator->fails()) {
            return ['status' => FALSE, 'mensagem' => $validator];
        }

        //Validações EXTRAS
        $errors = [];
        if (isset($dados['id'])) {
            $linguagem = Linguagem::where('nome', '=', $dados['nome'])->where('id', '<>', $dados['id'])->first();
            if ($linguagem) { $errors += ['nome' => 'Linguagem já cadastrada']; }
            $linguagem = Linguagem::where('url', '=', $dados['url'])->where('id', '<>', $dados['id'])->first();
            if ($linguagem) { $errors += ['url' => 'Url já cadastrada']; }
        } else {
            $linguagem = Linguagem::where('nome', '=', $dados['nome'])->first();
            if ($linguagem) { $errors += ['nome' => 'Linguagem já cadastrada']; }
            $linguagem = Linguagem::where('url', '=', $dados['url'])->first();
            if ($linguagem) { $errors += ['url' => 'Url já cadastrada']; }
        }

        if(count($errors) > 0) {
            return ['status' => FALSE, 'mensagem' => $errors];
        }

        //Cria ou Grava a Linguagem
        if (isset($dados['id'])) {
            $linguagem = Linguagem::where('id', '=', $dados['id'])->first();
        } else {
            $linguagem = new Linguagem();
        }

        $linguagem->nome    = $dados['nome'];
        $linguagem->url     = $dados['url'];

        try {
            $linguagem->save();
            return ['status' => TRUE, 'mensagem' => "Linguagem " . $linguagem->nome . " salva!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao salvar a linguagem. Avise a Área de TI."];
        }
    }

    /**
     * Apaga a Linguagem
     */
     public static function apagar($id) {
         $linguagem = Linguagem::where('id','=', $id)->first();
         if (!$linguagem) {
             return ['status' => FALSE, 'mensagem' => "Linguagem não encontrada!"];
         }

         try {
             $linguagem->delete();
             return ['status' => TRUE, 'mensagem' => "Linguagem apagada!"];
         } catch (\Illuminate\Database\QueryException $e) {
             return ['status' => FALSE, 'mensagem' => "Esta Linguagem está associada a um ou mais registros!"];
         }
     }
}
