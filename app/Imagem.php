<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class Imagem extends Model
{
    protected $table = 'imagens';
    protected $fillable = ['linguagem_id', 'usuario_id', 'post_id', 'nome', 'tipo', 'caminho', 'caminho_completo', 'altura', 'largura', 'tamanho', 'texto'];

    //Relaciona a Linguagem
    public function linguagem() {
        return $this->belongsTo(Linguagem::class);
    }

    //Relaciona o Usuário
    public function usuario() {
        return $this->belongsTo(Usuario::class);
    }

    //Relaciona o Post
    public function post() {
        return $this->belongsTo(Post::class);
    }

    public static function listar($linguagem_id = 0, $post_id = 0) {
        if($linguagem_id == 0) {
            $imagens = Imagem::orderBy('created_at','desc')->get();
        }else{
            if($post_id == 0) {
                $imagens = Imagem::where('linguagem_id','=', $linguagem_id)->orderBy('created_at','desc')->get();
            }else{
                $imagens = Imagem::where('linguagem_id','=', $linguagem_id)
                    ->where('post_id','=', $post_id)->orderBy('created_at','desc')->get();
            }
        }
        return $imagens;
    }

    public static function inserir($dados) {

        $imagem = new Imagem();
        $imagem->linguagem_id       = $dados['linguagem_id'];
        $imagem->usuario_id         = $dados['usuario_id'];
        $imagem->post_id            = $dados['post_id'];
        $imagem->nome               = $dados['nome'];
        $imagem->tipo               = $dados['tipo'];
        $imagem->caminho            = $dados['caminho'];
        $imagem->caminho_completo   = $dados['caminho_completo'];
        $imagem->altura             = $dados['altura'];
        $imagem->largura            = $dados['largura'];
        $imagem->tamanho            = $dados['tamanho'];
        $imagem->texto              = $dados['texto'];

        try {
            $imagem->save();
            return $imagem->id;
        } catch (\Illuminate\Database\QueryException $e) {
            return FALSE;
        }
    }

    public static function apagar($id) {
        $imagem = Imagem::where('id','=',$id)->first();
        if(file_exists ( public_path() . $imagem->caminho_completo )) {
            unlink( public_path() . $imagem->caminho_completo );
        }

        $imagem->delete();
        return TRUE;
    }

    public static function upload($imagem, $linguagem_id = 0, $post_id = 0) {

        $extensao               = $imagem->getClientOriginalExtension();
        $nomeArquivo            = $imagem->getClientOriginalName();
        $nomeArquivoSemExtensao = str_replace('.'.$extensao, '', $nomeArquivo);

        //Extensão da imagem
        if ($extensao <> 'jpg' && $extensao <> 'png' && $extensao <> 'jpeg'  ) {
            return " Imagem inválida. Somente aceito imagens de extensão jpg ou png";
        }

        $data = Carbon::now();
        $ano  = $data->year;
        $mes  = $data->month;

        $baseDestino        = '/img/'. $ano .'/' . $mes . '/';
        $pastaDestino       = public_path() . $baseDestino;

        //Pasta de Destino BASE
        if(!file_exists ( public_path() . '/img' )) {
            if(!mkdir(public_path() . '/img')) {
                return response()->json(['status' => 'FALSE', 'mensagem' => " Não foi possível criar a pasta /img/ . Avise a Área de TI"]);
            }
        }
        //Verifica se a pasta ANO existe
        if(!file_exists ( public_path() . '/img/' . $ano . '/' )) {
            if(!mkdir(public_path() . '/img/' . $ano . '/')) {
               return response()->json(['status' => 'FALSE', 'mensagem' => " Não foi possível criar a pasta /img/' . $ano . '/ . Avise a Área de TI"]);
            }
        }
        //Verifica se a pasta MÊS existe.
        if(!file_exists ( public_path() . '/img/' . $ano . '/' . $mes . '/' )) {
            if(!mkdir(public_path() . '/img/' . $ano . '/' . $mes . '/')) {
               return response()->json(['status' => 'FALSE', 'mensagem' => " Não foi possível criar a pasta /img/' . $ano . '/' . $mes .'/ . Avise a Área de TI"]);
            }
        }

        //Para evitar que algum que os arquivos sejam sobrescritos caso já existam.
        $arquivosExistentes = glob($pastaDestino.$nomeArquivoSemExtensao.'*'.$extensao, GLOB_MARK);
        if(count($arquivosExistentes) > 0) {
            //nome arquivo = nome_arquivo-2.jpj por exemplo.
            $nomeArquivo = $nomeArquivoSemExtensao . '-' . (count($arquivosExistentes)+1) . '.' . $extensao;
        }

        $imagem->move($pastaDestino, $nomeArquivo);

        $dimensoes      = getimagesize($pastaDestino . $nomeArquivo);
        $largura        = $dimensoes[0];
        $altura         = $dimensoes[1];
        $tamanho        = filesize($pastaDestino . $nomeArquivo);
        //$tamanho               = filesize($imagem);

        $img = [];
        $img['linguagem_id']        = $linguagem_id;
        $img['usuario_id']          = session()->get('usuario_id');
        $img['post_id']             = $post_id;
        $img['nome']                = $nomeArquivo;
        $img['tipo']                = 'jpg'; //mime_content_type($imagem);
        $img['caminho']             = $baseDestino;
        $img['caminho_completo']    = $baseDestino . $nomeArquivo;
        $img['altura']              = $altura;
        $img['largura']             = $largura;
        $img['tamanho']             = $tamanho;
        $img['texto']               = '';

        $retorno = Imagem::inserir($img);

        return $retorno;

    }

    public static function dados($id) {
        $imagem = Imagem::where('id','=',$id)->first();
        if($imagem) {
            return $imagem;
        }
        return FALSE;
    }

    public static function criaThumbnail($id, $tamanho = 150) {

        if(!file_exists ( public_path() . '/img' )) {
            if(!mkdir(public_path() . '/img')) {
                return FALSE;
            }
        }
        //Verifica se a pasta THUMB
        if(!file_exists ( public_path() . '/img/thumb/' )) {
            if(!mkdir(public_path() . '/img/thumb/')) {
               return FALSE;
            }
        }
        $imagem = Imagem::where('id','=', $id)->first();
        if(!$imagem) {
            return FALSE;
        }
        $img = Image::make(public_path() . '/' .$imagem->caminho_completo);
        $img->fit($tamanho);
        $img->save(public_path() . '/img/thumb/' . $imagem->id . '.jpg');

        return TRUE;
    }

    public static function apagaThumbnail($id) {
        if(file_exists(public_path() . '/img/thumb/' . $id . '.jpg')) {
            unlink(public_path() . '/img/thumb/' . $id . '.jpg');
        }

        return TRUE;
    }
}
