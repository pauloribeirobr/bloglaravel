<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Linguagem;
use App\Usuario;
use App\Post;
use App\Imagem;

class ImagemController extends Controller
{
    public function listar(Request $request) {

        if ($request->isMethod('post')) {
            session()->put('filtroLinguagem' , $request->linguagem);
            session()->put('filtroPost' , $request->post);
        }

        $linguagens = Linguagem::listar();
        $posts   = Post::listar(session()->get('filtroLinguagem'));
        $imagens = Imagem::listar(session()->get('filtroLinguagem'), session()->get('filtroPost'));


        return view('admin.imagem.listarImagens', [
            'imagens'       => $imagens,
            'linguagens'    => $linguagens,
            'posts'         => $posts
        ]);
    }

    public function upload(Request $request) {

        $imagem             = $request->imagem;
        $retorno            = Imagem::upload($imagem, session()->get('filtroLinguagem'), session()->get('filtroPost'));

        if(!$retorno) {
            return response()->json(['status' => 'FALSE', 'mensagem' => "Não foi possível salvar a imagem!"]);
        }
        $retornoThumb = Imagem::criaThumbnail($retorno);

        return response()->json(['status' => 'TRUE', 'mensagem' => "Imagem enviada com sucesso"]);
    }

    public function dados(Request $request, $id = -1) {
        if ($request->isMethod('get')) {
            $imagem         = Imagem::dados($id);
            $linguagens     = Linguagem::listar();
            $usuarios       = Usuario::listar();
            if(!$imagem) {
                return redirect()->route('_imagens');
            }
            return view('admin.imagem.dadosImagem', [
                'imagem'        => $imagem,
                'linguagens'    => $linguagens,
                'usuarios'      => $usuarios
            ]);
        }

        if ($request->isMethod('post')) {

            $imagem                 = Imagem::dados($id);
            $imagem->texto          = $request->texto;
            $imagem->linguagem_id   = $request->linguagem;
            $imagem->usuario_id     = $request->usuario;

            $imagem->save();

            return redirect()->route('_imagens_dados', ['id' => $imagem->id]);
        }

    }

    public function apiGetDados($id) {
        $imagem = Imagem::dados($id);
        return $imagem->toJson();
    }

    public function apagar($id) {
        Imagem::apagaThumbnail($id);
        Imagem::apagar($id);

        return redirect()->route('_imagens');
    }
}
