<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    public function index() {
        return view('blog.index');
    }

    public function mostraPost($url) {
        $post = Post::where('url','=',$url)->first();
        return view('blog.post', ['post' => $post]);
    }
}
