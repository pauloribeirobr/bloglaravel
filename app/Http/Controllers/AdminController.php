<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Validator;
use App\Usuario;
use App\Http\Requests;

class AdminController extends Controller
{
    public function index() {
        return view('admin.index');
    }

    /**
    * Efetua login
    */
    public function login(Request $request) {
        //get - mostra a view de login SE o usuário não estiver conectado
        if ($request->isMethod('get')) {
            if (session()->has('conectado')) {
                return redirect()->route('_admin');
            }
            return view('admin.login.login');
        }

        //post - processa o preenchimento
        if ($request->isMethod('post')) {
            $messages = [
                'required' => 'Preencha o campo :attribute',
            ];

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'senha' => 'required',
            ], $messages);

            //Se deu qualquer pau retorna a tela anterior e mostra o problema
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
            }

            //aqui valida pelo Repository
            $retornoLogin = Usuario::login($request->email, $request->senha);

            if ($retornoLogin['status']) {
                return redirect()->route('_admin');
            } else {
                return redirect()->route('_login')->with('alertaERRO', $retornoLogin['mensagem']);
            }
        }
    }

    /**
    * Recupera Senha
    */
    public function recuperaSenha(Request $request) {
        if ($request->isMethod('get')) {
            if (session()->has('conectado')) {
                return redirect()->route('home');
            }
            return view('admin.login.recuperaSenha');
        }

        if ($request->isMethod('post')) {
            $messages = [
                'required' => 'Preencha o campo :attribute',
                'email'     => 'Preencha com um email válido'
            ];

            $validator = Validator::make($request->all(), [
                'email' => 'required | email'
            ], $messages);

            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
            }

            $retornoEmail = Usuario::recuperaSenha($request->email);

            if ($retornoEmail['status']) {
                return redirect()->route('_recuperaSenha')->with('alertaOK', $retornoEmail['mensagem']);;
            } else {
                return redirect()->route('_recuperaSenha')->with('alertaERRO', $retornoEmail['mensagem']);
            }
        }
    }

    /*
    * Reset de senha
    */
    public function reset(Request $request) {
        //Captura o token enviado
        $token = $request->token;

        $retornoReset = Usuario::reset($token);

        if ($retornoReset['status']) {
            return redirect()->route('_trocar_senha')->with('alertaOK', 'Efetue a troca da senha');;
        } else {
            return redirect()->route('_recuperaSenha')->with('alertaERRO', $retornoReset['mensagem']);
        }
    }

    /**
    * Trocar Senha
    */
    public function trocarSenha(Request $request) {


        if ($request->isMethod('get')) {
            return view('admin.usuarios.trocarSenha');
        }

        if ($request->isMethod('post')) {

            $messages = [
                'required' => 'O campo :attribute precisa estar preenchido'
            ];

            $validator = Validator::make($request->all(), [
                'senha' => 'required',
                'senhaRepetida' => 'required',
            ], $messages);

            //Se alguma validação deu erro, retorna
            if ($validator->fails()) {
                return redirect()
                ->back()
                ->withErrors($validator);
            }

            $retorno = Usuario::trocaSenha(Session::get('usuario_id'), $request->senha, $request->senhaRepetida);

            if ($retorno['status']) {
                return redirect()->route('_admin')->with('alertaOK', $retorno['mensagem']);
            } else {

                return redirect()->route('_trocar_senha')->with('alertaERRO', $retorno['mensagem']);
            }
        }
    }

    /**
    * Efetuar Logout
    */
    public function logout() {
        Session::flush();
        return redirect()->route('_admin');
    }
}
