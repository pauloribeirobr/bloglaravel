<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Linguagem;
use Validator;

class LinguagemController extends Controller
{
    /**
    * Listar
    */
    public function listar(Request $request) {

        $linguagens = Linguagem::listar();

        return view('admin.linguagem.listarLinguagens', [
            'linguagens'    => $linguagens
        ]);
    }

    /**
     * Dados
     */
    public function dados(Request $request, $id = -1) {

        if ($request->isMethod('get')) {
            if ($id == -1) {
                return view('admin.linguagem.dadosLinguagem');
            }else{
                $linguagem = Linguagem::dados($id);
                return view('admin.linguagem.dadosLinguagem', [
                    'linguagem'    => $linguagem
                ]);
            }
        }

        if ($request->isMethod('post')) {

            $linguagem = [];
            if ($request->has('id')) {
                $linguagem['id'] = $request->id;
            }
            $linguagem['nome'] = $request->nome;
            $linguagem['url']  = $request->url;

            $retorno = Linguagem::gravar($linguagem);

            if ($retorno['status']) {
                return redirect()->route('_linguagens')->with('alertaOK', $retorno['mensagem']);
            } else {
                return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
            }
        }
    }

    /**
     * Apagar
     */
     public function apagar($id) {
         $retorno = Linguagem::apagar($id);

         if ($retorno['status']) {
             return redirect()->route('_linguagens')->with('alertaOK', $retorno['mensagem']);
         } else {
             return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
         }
     }
}
