<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Linguagem;
use App\PostCategoria;

class CategoriaController extends Controller
{

    public function listar(Request $request) {

        if ($request->isMethod('post')) {
            session()->put('filtroLinguagem' , $request->linguagem);
        }

        $linguagens = Linguagem::listar();
        $categorias = Categoria::listar(session()->get('filtroLinguagem'));

        return view('admin.categoria.listarCategorias', [
            'categorias'    => $categorias,
            'linguagens'    => $linguagens
        ]);
    }

    public function dados(Request $request, $id = -1) {

        $linguagens     = Linguagem::listar();
        $posts          = PostCategoria::listarPorCategoria($id);

        if ($request->isMethod('get')) {
            if ($id == -1) {
                return view('admin.categoria.dadosCategoria', [
                    'linguagens'    => $linguagens,
                    'posts'         => $posts
                ]);
            }else{
                $categoria = Categoria::dados($id);
                return view('admin.categoria.dadosCategoria', [
                    'categoria'     => $categoria,
                    'linguagens'    => $linguagens,
                    'posts'         => $posts
                ]);
            }
        }

        if ($request->isMethod('post')) {

            $categoria = [];
            if ($request->has('id')) {
                $categoria['id'] = $request->id;
            }
            $categoria['nome']      = $request->nome;
            $categoria['url']       = $request->url;
            $categoria['linguagem'] = $request->linguagem;

            $retorno = Categoria::gravar($categoria);

            if ($retorno['status']) {
                return redirect()->route('_categorias')->with('alertaOK', $retorno['mensagem']);
            } else {
                return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
            }
        }
    }

    public function apagar($id) {
        $retorno = Categoria::apagar($id);

        if ($retorno['status']) {
            return redirect()->route('_categorias')->with('alertaOK', $retorno['mensagem']);
        } else {
            return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
        }
    }

    public function acima($id) {
        Categoria::acima($id);
        return redirect()->route('_categorias');
    }

    public function abaixo($id) {
        Categoria::abaixo($id);
        return redirect()->route('_categorias');
    }
}
