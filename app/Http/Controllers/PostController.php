<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Linguagem;
use App\Usuario;
use App\Categoria;
use App\Post;

class PostController extends Controller
{
    public function listar(Request $request) {

        if ($request->isMethod('post')) {
            session()->put('filtroLinguagem' , $request->linguagem);
        }

        $linguagens = Linguagem::listar();
        $posts = Post::listar(session()->get('filtroLinguagem'));

        return view('admin.post.listarPosts', [
            'posts'         => $posts,
            'linguagens'    => $linguagens
        ]);
    }

    public function dados(Request $request, $id = -1) {

        $linguagens = Linguagem::listar();
        $usuarios   = Usuario::listar();

        if ($request->isMethod('get')) {
            if ($id == -1) {
                return view('admin.post.dadosPost', [
                    'linguagens'    => $linguagens,
                    'usuarios'      => $usuarios
                ]);
            }else{
                $post = Post::dados($id);
                return view('admin.post.dadosPost', [
                    'post'          => $post,
                    'linguagens'    => $linguagens,
                    'usuarios'      => $usuarios
                ]);
            }
        }

        if ($request->isMethod('post')) {

            $post = [];
            if ($request->has('id')) {
                $post['id'] = $request->id;
            }
            $post['nome']       = $request->nome;
            $post['url']        = $request->url;
            $post['linguagem']  = $request->linguagem;
            $post['usuario']    = $request->usuario;
            $post['resumo']     = $request->resumo;
            $post['texto']      = $request->texto;
            if ($request->has('publicado')) {
                $post['publicado'] = TRUE;
            }else{
                $post['publicado'] = FALSE;
            }

            $retorno = Post::gravar($post);

            if ($retorno['status']) {
                return redirect()->route('_posts_dados',['id' => $retorno['id']])->with('alertaOK', $retorno['mensagem']);
                //return redirect()->route('_posts')->with('alertaOK', $retorno['mensagem']);
            } else {
                return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
            }
        }
    }

    public function apagar($id) {
        $retorno = Post::apagar($id);

        if ($retorno['status']) {
            return redirect()->route('_posts')->with('alertaOK', $retorno['mensagem']);
        } else {
            return redirect()->back()->withErrors($retorno['mensagem'])->withInput();
        }
    }

    public function acima($id) {
        Post::acima($id);
        return redirect()->route('_posts');
    }

    public function abaixo($id) {
        Post::abaixo($id);
        return redirect()->route('_posts');
    }
}
