<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\PostCategoria;

class PostCategoriaController extends Controller
{
    //Lista todas as categorias da linguagem e marca as que estão no Post
    public function apiListar($linguagem_id = -1, $post_id = -1) {

        $categorias = PostCategoria::listarCategorias($linguagem_id, $post_id);

        return $categorias;
    }

    //Marca/Desmarca a Categoria do Post
    public function apiCheck(Request $request) {

        $post_id        = $request->post_id;
        $categoria_id   = $request->categoria_id;

        $postCategoria = PostCategoria::dados($post_id, $categoria_id);
        if(!$postCategoria) {
            PostCategoria::inserir($post_id, $categoria_id);
        }else{
            PostCategoria::remover($post_id, $categoria_id);
        }

        return;
    }

    public function acima($id) {
        $postCategoria = PostCategoria::find($id);
        $categoria = Categoria::find($postCategoria->categoria_id);
        PostCategoria::acima($id);
        return redirect()->route('_categorias_dados', ['id' => $categoria->id]);
    }

    public function abaixo($id) {
        $postCategoria = PostCategoria::find($id);
        $categoria = Categoria::find($postCategoria->categoria_id);
        PostCategoria::abaixo($id);
        return redirect()->route('_categorias_dados', ['id' => $categoria->id]);
    }
}
