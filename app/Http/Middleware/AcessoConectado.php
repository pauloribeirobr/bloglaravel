<?php
/**
 * Paulo Ribeiro
 */
namespace App\Http\Middleware;

use Closure;
use Session;

class AcessoConectado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Verifica se a Session Conectado Existe.
         * Se existir continua o processamento, senão redireciona para a página de login
         */

        //return print_r(Session::all());

        if (Session::has('conectado')) {
            return $next($request);
        }else{
            return redirect('/admin/login');
        }
    }
}
