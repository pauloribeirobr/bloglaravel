<?php

namespace App;

use Hash;
use Illuminate\Database\Eloquent\Model;
use Mail;
use DB;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $fillable = ['nome', 'email', 'senha'];
    protected $hidden = ['senha'];

    /**
    * Lista todas os Usuarios
    */
    public static function listar() {
        $usuarios = Usuario::orderBy('nome', 'asc')->get();
        return $usuarios;
    }

    /**
    * Abre os dados do Usuário
    */
    public static function dados($id) {
        $usuario = Usuario::where('id', '=', $id)->first();
        return $usuario;
    }

    /**
    * Grava o usuário (tanto incluir quanto alterar)
    */
    public static function gravar($request) {

        //Validações EXTRAS
        if ($request->has('id')) {

            //Pesquisa se o login já está cadastrado em outro usuário
            $usuario = Usuario::where('login', '=', $request->login)
            ->where('id', '<>', $request->id)
            ->first();
            if ($usuario) {
                return ['status' => FALSE, 'mensagem' => "Este login já está cadastrado em outro usuário!"];
            }

            //Pesquisa se o email já está cadastrado em outro usuário
            $usuario = Usuario::where('email', '=', $request->email)
            ->where('id', '<>', $request->id)
            ->first();
            if ($usuario) {
                return ['status' => FALSE, 'mensagem' => "Este email já está cadastrado em outro usuário!"];
            }
        } else {

            //Se for um NOVO usuário verifica duplicidade de login
            $usuario = Usuario::where('login', '=', $request->login)->first();
            if ($usuario) {
                return ['status' => FALSE, 'mensagem' => "Login já está cadastrado em outro usuário!"];
            }

            //Se for um NOVO usuário verifica duplicidade de email
            $usuario = Usuario::where('email', '=', $request->email)->first();
            if ($usuario) {
                return ['status' => FALSE, 'mensagem' => "Email já está cadastrado em outro usuário!"];
            }
        }

        // Preencher senha em caso de novo usuário
        if (!$request->has('id') && $request->senha == NULL) {
            return ['status' => FALSE, 'mensagem' => "Digite uma senha!"];
        }

        // Verificar se as senhas estão iguais
        if ($request->senha <> NULL) {
            if ($request->senha <> $request->senhaRepetida) {
                return ['status' => FALSE, 'mensagem' => "As 2 senhas precisam ser iguais!"];
            }
        }

        //Cria ou Grava o usuário
        if ($request->has('id')) {
            $usuario = Usuario::where('id', '=', $request->id)->first();
        } else {
            $usuario = new Usuario();
        }

        $usuario->login    = $request->login;
        $usuario->nome     = $request->nome;
        $usuario->email    = $request->email;

        if ($request->senha <> NULL) {
            $usuario->senha = bcrypt($request->senha);
        }

        if (!$request->has('id')) {
            $usuario->ativo = TRUE;
        } else {
            if ($request->ativo == 'ativo') {
                $usuario->ativo = TRUE;
            } else {
                $usuario->ativo = FALSE;
            }
        }

        try {
            $usuario->save();
            return ['status' => TRUE, 'mensagem' => "Dados de " . $usuario->nome . " salvos!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao salvar o usuário. Avise a Área de TI."];
        }
    }

    /* No processo de login verifica:
    * 1 - Se a combinação usuario/senha ou usuário/email existe
    * 2 - Se o usuário está ativo
    *
    * Após... registra TODAS as variaveis de ambiente para uso do Sistema -- SESSOES
    */
    public static function login($email, $senha = '', $automatico = FALSE) {
        $usuario = Usuario::where('login', '=', $email)
        ->orWhere('email', '=', $email)
        ->first();

        if (!$usuario) {
            return ['status' => FALSE, 'mensagem' => "Usuário / Senha inválidos!"];
        }

        if (!Hash::check($senha, $usuario->senha) && $automatico == FALSE) {
            return ['status' => FALSE, 'mensagem' => "Usuário / Senha inválidos!"];
        }

        if (!$usuario->ativo) {
            return ['status' => FALSE, 'mensagem' => "Usuário Bloqueado! Entre em contato com o Departamento de TI."];
        }

        //Cria as Sessões que serão usadas para Controle do Sistema
        session()->put('conectado'              , TRUE);
        session()->put('usuario_id'             , $usuario->id);
        session()->put('usuario_nome'           , $usuario->nome);
        session()->put('termoPesquisa'          , '');

        //filtragem genérica
        session()->put('ordemColuna'            , 'lojas.nome');
        session()->put('ordem'                  , 'asc');
        session()->put('termoPesquisa'          , '');

        //Filtro Linguagem e de Post
        session()->put('filtroLinguagem' , 0);
        session()->put('filtroPost' , 0);

        return ['status' => TRUE, 'mensagem' => "Usuário Logado com sucesso!"];
    }

    /**
    * Efetue o Logout do Sistema
    */
    public static function logout() {
        session()->flush();

        return TRUE;
    }

    /**
    * Troca a senha
    */
    public static function trocaSenha($idUsuario, $senhaNova = '', $senhaNovaRepetida = '') {

        //Verifica se as senhas são iguais
        if (trim($senhaNova) <> trim($senhaNovaRepetida)) {
            return ['status' => FALSE, 'mensagem' => "As senhas precisam ser iguais!"];
        }

        //Localiza o Usuário
        $usuario = Usuario::where('id', '=', $idUsuario)->first();

        if (!$usuario) {
            return ['status' => FALSE, 'mensagem' => "Usuário não encontrado!"];
        }

        $usuario->senha = bcrypt($senhaNova);

        try {
            $usuario->save();
            return ['status' => TRUE, 'mensagem' => "Senha alterada com sucesso!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao mudar a senha. Avise a Área de TI. Erro: "];
        }
    }

    /*
    * Gera o Email de Recuperação de Senha
    */
    public static function recuperaSenha($email) {
        //Verifica se o email existe
        $usuario = Usuario::where('email','=', $email)->first();

        if (!$usuario) {
            return ['status' => FALSE, 'mensagem' => "Email não encontrado!"];
        }

        //Se o usuário estiver bloqueado...
        if(!$usuario->ativo) {
            return ['status' => FALSE, 'mensagem' => "Seu usuário está bloqueado!. Entre em contato com a Área de TI"];
        }

        //Cria o email de envio
        $token = str_random(70);

        $link = url('/reset/' . $token);

        try {
            //Insere o token
            DB::table('recupera_senha')->insert([
                'email'        => $usuario->email,
                'token'        => $token,
                'created_at'   => \Carbon\Carbon::now()->toDateTimeString()
            ]);

            //E aqui envia o email
            Mail::send('admin.login._emailRecuperaSenha', [
                'link' => $link,
                'email' => $usuario->email
            ],function($message) use ($link, $email) {
                $message->subject('[pRibeiro.net] - Recuperação de Senha');
                $message->from('pauloribeirobr@gmail.com', '[Paulo Ribeiro]');
                $message->to($email);
            });
            return ['status' => TRUE, 'mensagem' => "Email enviado com sucesso! Verifique."];

        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao enviar o email. Avise a Área de TI. Erro: "];
        }
    }

    /*
    * Reset de Senha
    */
    public static function reset($token) {
        //Verifica se o Token é inválido
        $recuperaSenha = DB::table('recupera_senha')->where('token','=', $token)->first();

        if(!$recuperaSenha) {
            return ['status' => FALSE, 'mensagem' => "Link de recuperação inválido! Tente gerar novo link."];
        }

        $usuario = Usuario::where('email','=', $recuperaSenha->email)->first();

        if(!$usuario) {
            return ['status' => FALSE, 'mensagem' => "Tive problemas em localizar seu usuário. Entre em contato com a Área de TI"];
        }

        //Se o usuário estiver bloqueado...
        if(!$usuario->ativo) {
            return ['status' => FALSE, 'mensagem' => "Seu usuário está bloqueado!. Entre em contato com a Área de TI"];
        }

        //Apaga todos os Tokens relacionados ao email
        DB::table('recupera_senha')->where('email','=', $usuario->email)->delete();

        //Faz o login do usuário.
        Usuario::login($usuario->email , '', TRUE);

        return ['status' => TRUE, 'mensagem' => "Usuário encontrado!"];
    }
}
