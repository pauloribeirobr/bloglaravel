<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use App\PostCategoria;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $fillable = ['nome', 'url', 'linguagem_id', 'ordem'];

    //Campo usado somente no relacionamento entre Categorias x Posts
    public function setIsPostAttribute($value) {
        $this->attributes['is_post'] = $value;
    }
    public function getIsPostAttribute($value) {
        return $value;
    }

    //Relaciona a Linguagem
    public function linguagem() {
        return $this->belongsTo(Linguagem::class);
    }

    public static function listar($linguagem_id = 0) {
        if($linguagem_id == 0) {
            $categorias = Categoria::orderBy('ordem', 'asc')->get();
        }else{
            $categorias = Categoria::where('linguagem_id','=',$linguagem_id)->orderBy('ordem', 'asc')->get();
        }

        return $categorias;
    }

    public static function dados($id) {
        $categoria = Categoria::where('id','=', $id)->first();
        return $categoria;
    }

    public static function gravar($dados) {

        $messages = [
            'required' => 'O campo :attribute precisa estar preenchido',
            'max' => 'O campo :attribute permite no máximo :max caracteres'
        ];

        $validator = Validator::make($dados, [
            'nome'      => 'required|max:20',
            'url'       => 'required|max:20',
            'linguagem' => 'required'
        ], $messages);

        //Se alguma validação deu erro, retorna
        if ($validator->fails()) {
            return ['status' => FALSE, 'mensagem' => $validator];
        }

        //Validações EXTRAS
        $errors = [];
        if (isset($dados['id'])) {
            $categoria = Categoria::where('nome', '=', $dados['nome'])
                ->where('id', '<>', $dados['id'])->where('linguagem_id', '=', $dados['linguagem'])->first();
            if ($categoria) { $errors += ['nome' => 'Categoria já cadastrada']; }
            $categoria = Categoria::where('url', '=', $dados['url'])
                ->where('id', '<>', $dados['id'])->where('linguagem_id', '=', $dados['linguagem'])->first();
            if ($categoria) { $errors += ['url' => 'Url já cadastrada']; }
        } else {
            $categoria = Categoria::where('nome', '=', $dados['nome'])
                ->where('linguagem_id', '=', $dados['linguagem'])->first();
            if ($categoria) { $errors += ['nome' => 'Categoria já cadastrada']; }
            $categoria = Categoria::where('url', '=', $dados['url'])
                ->where('linguagem_id', '=', $dados['linguagem'])->first();
            if ($categoria) { $errors += ['url' => 'Url já cadastrada']; }
        }

        if(count($errors) > 0) {
            return ['status' => FALSE, 'mensagem' => $errors];
        }

        if (isset($dados['id'])) {
            $categoria          = Categoria::where('id', '=', $dados['id'])->first();
        } else {
            $categoria          = new Categoria();
            $categoria->ordem   = (Categoria::where('linguagem_id','=', $dados['linguagem'])->count()) + 1;
        }

        $categoria->nome            = $dados['nome'];
        $categoria->url             = $dados['url'];
        $categoria->linguagem_id    = $dados['linguagem'];

        //Inserir a Categoria para o último item


        try {
            $categoria->save();
            return ['status' => TRUE, 'mensagem' => "Categoria " . $categoria->nome . " salva!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Problemas ao salvar a categoria. Avise a Área de TI. " .  $e->getMessage()];
        }
    }

    public static function apagar($id) {
        $categoria = Categoria::where('id','=', $id)->first();
        if (!$categoria) {
            return ['status' => FALSE, 'mensagem' => "Linguagem não encontrada!"];
        }

        try {
            $categoria->delete();
            return ['status' => TRUE, 'mensagem' => "Categoria apagada!"];
        } catch (\Illuminate\Database\QueryException $e) {
            return ['status' => FALSE, 'mensagem' => "Esta Categoria está associada a um ou mais registros!"];
        }
    }

    public static function acima($categoria_id) {
        $categoria = Categoria::find($categoria_id);
        $ordem = $categoria->ordem;
        $categoriaAnterior = Categoria::where('ordem','<', $ordem)
            ->where('linguagem_id','=', $categoria->linguagem_id)->orderBy('ordem','DESC')->first();
        if($categoriaAnterior) {
            $categoria->ordem = $categoriaAnterior->ordem;
            $categoriaAnterior->ordem = $ordem;
            $categoria->save();
            $categoriaAnterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }

    public static function abaixo($categoria_id) {
        $categoria = Categoria::find($categoria_id);
        $ordem = $categoria->ordem;
        $categoriaPosterior = Categoria::where('ordem','>', $ordem)
            ->where('linguagem_id','=', $categoria->linguagem_id)->orderBy('ordem','ASC')->first();
        if($categoriaPosterior) {
            $categoria->ordem = $categoriaPosterior->ordem;
            $categoriaPosterior->ordem = $ordem;
            $categoria->save();
            $categoriaPosterior->save();
            return ['status' => TRUE];
        }
        return ['status' => FALSE];
    }

}
