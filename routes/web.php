<?php

//Usuários
Route::match(['get', 'post'], 'admin/login', ['as' => '_login', 'uses' => 'AdminController@login']);
Route::match(['get', 'post'], 'admin/recupera-senha', ['as' => '_recuperaSenha', 'uses' => 'AdminController@recuperaSenha']);
Route::get('/reset/{token?}', ['as' => '_reset', 'uses' => 'AdminController@reset']);

/*
* Rotas disponÍveis somente para USUÁRIO AUTENTICADO.
*/
Route::group(['middleware' => 'acesso', 'prefix' => 'admin'], function() {
    Route::get('/', ['as' => '_admin', 'uses' => 'AdminController@index']);
    Route::get('logout', ['as' => '_logout', 'uses' => 'AdminController@logout']);

    //Linguagens
    Route::get('linguagens', ['as' => '_linguagens', 'uses' => 'LinguagemController@listar']);
    Route::match(['get', 'post'], 'linguagens/incluir', ['as' => '_linguagens_incluir', 'uses' => 'LinguagemController@dados']);
    Route::match(['get', 'post'], 'linguagens/dados/{id}', ['as' => '_linguagens_dados', 'uses' => 'LinguagemController@dados']);
    Route::match(['get', 'post'], 'linguagens/apagar/{id}', ['as' => '_linguagens_apagar', 'uses' => 'LinguagemController@apagar']);

    //Categorias
    Route::match(['get', 'post'], 'categorias', ['as' => '_categorias', 'uses' => 'CategoriaController@listar']);
    Route::match(['get', 'post'], 'categorias/incluir', ['as' => '_categorias_incluir', 'uses' => 'CategoriaController@dados']);
    Route::match(['get', 'post'], 'categorias/dados/{id}', ['as' => '_categorias_dados', 'uses' => 'CategoriaController@dados']);
    Route::match(['get', 'post'], 'categorias/apagar/{id}', ['as' => '_categorias_apagar', 'uses' => 'CategoriaController@apagar']);
    Route::get('categorias/acima/{id}', ['as' => '_categorias_acima', 'uses' => 'CategoriaController@acima']);
    Route::get('categorias/abaixo/{id}', ['as' => '_categorias_abaixo', 'uses' => 'CategoriaController@abaixo']);

    //Posts
    Route::match(['get', 'post'], 'posts', ['as' => '_posts', 'uses' => 'PostController@listar']);
    Route::match(['get', 'post'], 'posts/incluir', ['as' => '_posts_incluir', 'uses' => 'PostController@dados']);
    Route::match(['get', 'post'], 'posts/dados/{id}', ['as' => '_posts_dados', 'uses' => 'PostController@dados']);
    Route::match(['get', 'post'], 'posts/apagar/{id}', ['as' => '_posts_apagar', 'uses' => 'PostController@apagar']);
    Route::get('posts/acima/{id}', ['as' => '_posts_acima', 'uses' => 'PostController@acima']);
    Route::get('posts/abaixo/{id}', ['as' => '_posts_abaixo', 'uses' => 'PostController@abaixo']);
    Route::get('posts/categorias/acima/{id}', ['as' => '_posts_categorias_acima', 'uses' => 'PostCategoriaController@acima']);
    Route::get('posts/categorias/abaixo/{id}', ['as' => '_posts_categorias_abaixo', 'uses' => 'PostCategoriaController@abaixo']);

    //Imagens
    Route::match(['get', 'post'], 'imagens', ['as' => '_imagens', 'uses' => 'ImagemController@listar']);
    Route::match(['post'], 'imagens/upload', ['as' => '_imagens_upload', 'uses' => 'ImagemController@upload']);
    Route::match(['get', 'post'], 'imagens/incluir', ['as' => '_imagens_incluir', 'uses' => 'ImagemController@dados']);
    Route::match(['get', 'post'], 'imagens/dados/{id}', ['as' => '_imagens_dados', 'uses' => 'ImagemController@dados']);
    Route::match(['get', 'post'], 'imagens/apagar/{id}', ['as' => '_imagens_apagar', 'uses' => 'ImagemController@apagar']);

    //Apis
    Route::match(['get'], 'imagens/api/dados/{id}', ['as' => '_imagens_api_dados', 'uses' => 'ImagemController@apiGetDados']);
    Route::match(['get'], 'categorias/api/{linguagem_id?}/{post_id?}', ['as' => '_api_categorias', 'uses' => 'PostCategoriaController@apiListar']);
    Route::match(['post'], 'categorias/api/check', ['as' => '_api_categorias_check', 'uses' => 'PostCategoriaController@apiCheck']);

});

//Blog - Navegação
Route::get('/', ['as' => '_home', 'uses' => 'HomeController@index']);
Route::get('{url}', ['as' => '_post', 'uses' => 'HomeController@mostraPost']);
